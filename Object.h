//
// Created by Lettle on 2021/6/26.
//

#ifndef TERMINAL_MC_OBJECT_H
#define TERMINAL_MC_OBJECT_H

/* y
 * ^
 * |
 * |
 * |
 * ------------------> x
 */


class Object{
public:
    int x;
    int y;
    char body;
    char head;
    bool set(int,int,char,char);
};



#endif //TERMINAL_MC_OBJECT_H
