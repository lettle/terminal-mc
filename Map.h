//
// Created by Lettle on 2021/6/26.
//

#ifndef TERMINAL_MC_MAP_H
#define TERMINAL_MC_MAP_H

#include "globals.h"


class Map {
public:
    char map[mapHeight][mapLength];
    Map();
    bool showMap();
};


#endif //TERMINAL_MC_MAP_H
