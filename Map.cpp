//
// Created by Lettle on 2021/6/26.
//

#include "globals.h"
#include "Map.h"
#include <iostream>
using namespace std;

Map::Map()
{
    for(int i=0;i<mapHeight;i++){
        for(int ii=0;ii<mapLength;ii++){
            map[i][ii] = '0';
        }
    }
}

bool Map::showMap()
{
    for(int i=0;i<mapHeight;i++){
        for(int ii=0;ii<mapLength;ii++){
            cout<< map[i][ii];
        }
        cout << endl;
    }
    return true;
}
